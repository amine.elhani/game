let mainState = {

    preload: function () {  // Qua vengono precaricati gli assets
        //sprite del gioco
        game.load.image('player', 'assets/player.png');
        game.load.image('wall', 'assets/wall.png');
        game.load.image('coin', 'assets/coin.png');
        game.load.image('lava', 'assets/lava.png');
    },

    create: function () {  // Qua viene creato il gioco

        // imposta colore sfondo
        game.stage.backgroundColor = '#A0CFEC';

        // Avvia il tipo di fisica del gioco (movimenti, collisioni....)
        game.physics.startSystem(Phaser.Physics.ARCADE);

        // aggiungere l'engine fisico a tutti gli oggetti del gioco
        game.world.enableBody = true;

        // Variabile che memorizza i tasti di movimento premuti
        this.cursor = game.input.keyboard.createCursorKeys();

        //Crea il giocatore
        this.player = game.add.sprite(70, 100, 'player');

        //Crea la gravità
        this.player.body.gravity.y = 600;

        // crea 3 gruppi che contengono gli oggetti
        this.walls = game.add.group();
        this.coins = game.add.group();
        this.lavas = game.add.group();

        //CREA IL LIVELLO DEL GIOCO
        //come verrano creati i livelli x = muro, o = coin, ! = lava.
        var level = [
            'xxxxxxxxxxxxxxxxxxxxxx',
            '!         !          x',
            '!                 o  x',
            '!         o          x',
            '!                    x',
            '!     o   !    x     x',
            'xxxxxxxxxxxxxxxx!!!!!x',
        ];
        // crea il livello con l'array
        for (var i = 0; i < level.length; i++) {
            for (var j = 0; j < level[i].length; j++) {

                // crea muri
                if (level[i][j] == 'x') {
                    var wall = game.add.sprite(30 + 20 * j, 30 + 20 * i, 'wall');
                    this.walls.add(wall);
                    wall.body.immovable = true;
                }

                // crea monete
                else if (level[i][j] == 'o') {
                    var coin = game.add.sprite(30 + 20 * j, 30 + 20 * i, 'coin');
                    this.coins.add(coin);
                }

                // crea nemici
                else if (level[i][j] == '!') {
                    var lava = game.add.sprite(30 + 20 * j, 30 + 20 * i, 'lava');
                    this.lavas.add(lava);
                }
            }
        }

    },

    update: function () {  // Qua viene aggiornato 60 volte al secondo

        // il giocatore si muove se viene premuto un tasto freccia

        if (this.cursor.left.isDown)
            this.player.body.velocity.x = -200;
        else if (this.cursor.right.isDown)
            this.player.body.velocity.x = 200;
        else
            this.player.body.velocity.x = 0;

        // se il giocatore tocca il suolo: il giocatore salta
        if (this.cursor.up.isDown && this.player.body.touching.down)
            this.player.body.velocity.y = -250;

        //COLLISIONI
        // fa scontrare il player sui muri
        game.physics.arcade.collide(this.player, this.walls);

        // chiama la funzione "take coin" quando il player prende una moneta
        game.physics.arcade.overlap(this.player, this.coins, this.takeCoin, null, this);

        // chiama la funzione "restart" quando il player prende la lava
        game.physics.arcade.overlap(this.player, this.lavas, this.restart, null, this);
    },
    // Moneta raccolta
    takeCoin: function (player, coin) {
        coin.kill();
    },

    // Riavvia il gioco
    restart: function () {
        game.state.start('main');
    }
};

// Inizializza il gioco
let game = new Phaser.Game(500, 300);
game.state.add('main', mainState);
game.state.start('main');